import java.util.ArrayList;
import java.util.List;

public class Customer {
	
	private int frequentRenterPoints = 0;
	private double totalAmount = 0.00;

	public Customer(String name) {
		this.name = name;
	}

	public void addRental(Rental rental) {
		rentals.add(rental);
		frequentRenterPoints += rental.getFrequentRenterPoints();
		totalAmount += rental.getAmount();
	}

	public String getName() {
		return name;
	}

	public String statement() {
		String result = "Rental Record for " + getName() + "\n";

		for (Rental rental : rentals) {

			result += "\t" + rental.getMovie().getTitle() + "\t" + String.valueOf(rental.getAmount()) + "\n";
		}

		result += "You owed " + String.valueOf(getTotalAmount()) + "\n";
		result += "You earned " + String.valueOf(getTotalFrequentRenterPoints()) + " frequent renter points\n";

		return result;
	}
	
	public int getTotalFrequentRenterPoints() {
		return frequentRenterPoints;
	}
	
	public double getTotalAmount() {
		return totalAmount;
	}

	private String name;
	private List<Rental> rentals = new ArrayList<>();

}

	